Yii2 helpers
============
Yii2 helpers

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist oteixido/yii2-helpers "*"
```

or add

```
"oteixido/yii2-helpers": "*"
```

to the require section of your `composer.json` file.
